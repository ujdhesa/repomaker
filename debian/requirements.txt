# These requirements are not yet in Debian
fdroidserver==0.8
django-background-tasks==1.1.13
django-sass-processor
django-allauth
django-hvad>=1.8.0
django-tinymce
django-js-reverse
apache-libcloud>=2.0.0
pywebview
